const express = require("express");
const userRoutes = express.Router();
const userController = require("../controllers/userController");
const auth = require("../authorization");

// Create User
userRoutes.post("/createUser", userController.checkEmail, userController.createUser);
// Login User
userRoutes.post("/userLogin", userController.userLogin);
// Get User Cart
userRoutes.get("/usercart", auth.verifyToken, userController.getUserCart);

// Check Account if Admin  or not
userRoutes.get("/appraiseAccount", auth.verifyToken, userController.appraiseAccount);

userRoutes.patch("/updateUser/:userId", auth.verifyToken, userController.updateUser);

userRoutes.get("/", auth.verifyToken, userController.getAllUsers);

userRoutes.get("/getUserDetails", auth.verifyToken, userController.getOneUser);

userRoutes.patch("/addAddress", auth.verifyToken, userController.addAddress);
userRoutes.patch("/deleteAddress/:addressSlot", auth.verifyToken, userController.deleteAddress);

userRoutes.patch("/updateAddress/:addressSlot", auth.verifyToken, userController.updateAddress);

userRoutes.patch("/changeRole/:userId", auth.verifyToken, userController.changeRoleOfUser);

module.exports = userRoutes;
