require("dotenv").config();

const express = require("express");
const app = express();
const mongoose = require("mongoose");

const cors = require("cors");

mongoose.connect(process.env.MONGO_DB_LINK, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});
const db = mongoose.connection;
db.on("error", (error) => console.log(error.message));
db.once("open", () => console.log("Connected to DB"));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

const productRoutes = require("./routes/productRoutes");
app.use("/products", productRoutes);

const userRoutes = require("./routes/userRoutes");
app.use("/users", userRoutes);

const checkOutRoutes = require("./routes/checkOutRoutes");
app.use("/checkout", checkOutRoutes);

const orderRoutes = require("./routes/orderRoutes");
app.use("/orders", orderRoutes);

app.listen(process.env.PORT, console.log(`Connected to ${process.env.PORT}`));
