const mongoose = require("mongoose");

const typeString = { type: String, required: true };
const typeNumber = { type: Number, required: true };

const cartSchema = new mongoose.Schema(
    {
        items: [
            {
                productId: {
                    type: mongoose.Types.ObjectId,
                    refPath: "prodModel",
                },
                productImages: [{ type: String, required: true }],
                variations: { type: Array, required: true },
                productName: typeString,
                quantity: typeNumber,
                originalPrice: typeNumber,
                discount: typeString,
                totalPrice: typeNumber,
            },
        ],
        prodModel: { type: String, enum: ["Product", "Bundle"] },
        totalItems: { type: Number, default: 0 },
        cartOwner: { type: mongoose.Types.ObjectId, ref: "User" },
    },
    { timestamps: true }
);

module.exports = mongoose.model("Cart", cartSchema);
