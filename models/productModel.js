const mongoose = require("mongoose");

const typeString = { type: String, required: true };
const typeNumber = { type: Number, required: true, default: 0 };

const productSchema = new mongoose.Schema(
    {
        postedBy: { type: mongoose.Types.ObjectId, ref: "User" },
        productImages: { type: Array, default: [] },
        productName: typeString,
        description: typeString,
        originalPrice: { type: Number, required: true },
        discount: { type: String, default: "0%" },
        totalPrice: typeNumber,
        variations: [{ type: Object, default: {} }],
        stock: { type: Number, required: true },
        totalBuyCount: typeNumber,
        hasDiscount: { type: Boolean, default: false },
        isAvailable: { type: Boolean, default: true },
        productType: { type: String, default: "single" },
    },
    {
        timestamps: true,
    }
);

module.exports = mongoose.model("Product", productSchema);
