const mongoose = require("mongoose");

const typeString = { type: String, required: true };

const addressSchema = new mongoose.Schema({
    streetNumberAndName: typeString,
    brgyDistrict: typeString,
    cityMunicipality: typeString,
    country: typeString,
    postalCode: {
        type: Number,
        required: true,
    },
});

module.exports = mongoose.model("Address", addressSchema);
