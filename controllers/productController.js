const User = require("../models/userModel");
const Product = require("../models/productModel");
const BundleProduct = require("../models/bundleProductModel");
const Cart = require("../models/cartModel");
const auth = require("../authorization");

// ADMIN ONLY----------------------------------------------------------------
// Create/Post Product
module.exports.createProduct = async (req, res) => {
    const token = req.headers.authorization;
    const userData = auth.decodeToken(token);

    if (!userData.isAdmin) return res.json({ message: "You are not an Admin" });

    const input = req.body;

    try {
        const user = await User.findById(userData._id);
        if (!user) return res.json({ message: "User Not Found" });
        const newProduct = new Product({
            productImages: input.productImages,
            productName: input.productName,
            description: input.description,
            originalPrice: input.originalPrice,
            totalPrice: input.originalPrice,
            stock: input.stock,
            postedBy: user,
        });

        if (input.variations != null) {
            newProduct.variations = input.variations;
        }

        const hasImageInProperty = /Image/;
        for (const inputProperty in input) {
            if (hasImageInProperty.test(inputProperty)) {
                newProduct.productImages.push(input[inputProperty]);
            }
        }
        const postedProdcut = await newProduct.save();
        user.postedProducts.push(postedProdcut);
        await user.save();
        res.json(postedProdcut);
    } catch (error) {
        res.json({ message: error.message });
    }
};

// Create/Post Bundle Product
module.exports.createBundleProduct = async (req, res) => {
    const token = req.headers.authorization;
    const userData = auth.decodeToken(token);

    if (!userData.isAdmin) return res.json({ message: "You are not an Admin" });
    const productsToBeBundled = req.body;

    try {
        const user = await User.findById(userData._id);
        if (!user) return res.json({ message: "User Not Found" });

        const newBundle = new BundleProduct({
            bundleName: productsToBeBundled.bundleName,
            bundledItems: [],
            discount: productsToBeBundled.discount,
            stock: productsToBeBundled.stock,
            bundledBy: user,
        });

        let product;
        let preDiscPrice = 0;
        for (const reqProperty in productsToBeBundled) {
            if (reqProperty == "discount" || reqProperty == "stock" || reqProperty == "bundleName")
                continue;
            if (productsToBeBundled[reqProperty]) {
                product = await Product.findById(productsToBeBundled[reqProperty]);
            }
            if (product.stock < productsToBeBundled.stock || !product.isAvailable)
                return res.json({
                    message: `Not enough stock or unavailable: Product Id ${product.id}`,
                });

            newBundle.bundledItems.push(product);
            preDiscPrice += product.originalPrice;
            product.stock -= productsToBeBundled.stock;
            await product.save();
        }

        for (const productInBundle of newBundle.bundledItems) {
            const porductWhereImageIsTaken = await Product.findById(productInBundle._id);

            const productIdAndImages = {
                [productInBundle._id]: porductWhereImageIsTaken.productImages,
            };
            newBundle.bundleProductImages.push(productIdAndImages);
        }

        // EASIER WITH FRONTEND. For Fullstack, for more dynamic discounts, just convert string to number (and viceversa for displaying to client side)
        // Use converted string to number for calculations. For now, just stick with this.
        let discountPercetage;
        switch (productsToBeBundled.discount) {
            case "0%":
                discountPercetage = 0;
                break;
            case "5%":
                discountPercetage = 0.05;
                break;
            case "10%":
                discountPercetage = 0.1;
                break;
            case "15%":
                discountPercetage = 0.15;
                break;
            case "20%":
                discountPercetage = 0.2;
                break;
            default:
                return res.json({
                    message: "Discount can only be multiples of five from 0% to 20%",
                });
        }

        const discountedPrice = preDiscPrice - preDiscPrice * discountPercetage;

        if (discountPercetage == 0) {
            newBundle.hasDiscount = false;
            newBundle.totalPrice = newBundle.originalPrice;
        }

        if (discountPercetage != 0) newBundle.hasDiscount = true;

        newBundle.originalPrice = preDiscPrice;
        newBundle.discount = productsToBeBundled.discount;
        newBundle.totalPrice = discountedPrice;
        await newBundle.save();

        res.json({ message: "Bundle Created" });
    } catch (error) {
        res.json({ message: error.message });
    }
};

// GET/View ALL Products (Admin Only)
module.exports.getAllProducts = async (req, res) => {
    const token = req.headers.authorization;
    const userData = auth.decodeToken(token);

    if (!userData.isAdmin) return res.json({ message: "Not Admin" });
    // const productType = req.params.productType;

    try {
        let collectionToBrowse = Product;
        // if (productType == "single") collectionToBrowse = Product;
        // if (productType == "bundle") collectionToBrowse = BundleProduct;

        const allProducts = await collectionToBrowse.find();

        if (!allProducts.length) return res.json({ message: `There are no Products` });

        res.json(allProducts);
    } catch (error) {
        res.json({ message: error.message });
    }
};

// Update product (Admin Only)
module.exports.updateProduct = async (req, res) => {
    const token = req.headers.authorization;
    const userData = auth.decodeToken(token);
    const productId = req.params.productId;
    const properties = req.body;

    if (!userData.isAdmin)
        return res.json({
            message: "You are not Authorized to update this product",
        });
    const productType = req.params.productType;

    try {
        let collectionToBrowse = Product;
        // if (productType == "single") collectionToBrowse = Product;
        // if (productType == "bundle") collectionToBrowse = BundleProduct;

        const productToBeUpdated = await collectionToBrowse.findById(productId);
        if (!productToBeUpdated)
            return res.json({
                message: `No Product with ID ${productId} Found`,
            });

        for (const reqProperty in properties) {
            if (properties[reqProperty]) {
                productToBeUpdated[reqProperty] = properties[reqProperty];
            }
        }

        // EASIER WITH FRONTEND. For Fullstack, for more dynamic discounts, just convert string to number (and viceversa for displaying to client side)
        // Use converted string to number for calculations. For now, just stick with this.
        let discountPercetage;
        switch (properties.discount) {
            case "0%":
                discountPercetage = 0;
                break;
            case "5%":
                discountPercetage = 0.05;
                break;
            case "10%":
                discountPercetage = 0.1;
                break;
            case "15%":
                discountPercetage = 0.15;
                break;
            case "20%":
                discountPercetage = 0.2;
                break;
            default:
                return res.json({
                    message: "Discount can only be multiples of five from 0% to 20%",
                });
        }

        if (discountPercetage == 0) {
            productToBeUpdated.hasDiscount = false;
            productToBeUpdated.totalPrice = productToBeUpdated.originalPrice;
        }

        if (discountPercetage != 0) productToBeUpdated.hasDiscount = true;

        productToBeUpdated.totalPrice =
            productToBeUpdated.originalPrice - productToBeUpdated.originalPrice * discountPercetage;

        const updatedProduct = await productToBeUpdated.save();
        res.json(updatedProduct);
    } catch (error) {
        res.json({ message: error.message });
    }
};

// Archive or Unarchive Product (Admin only)
module.exports.archiveOrUnarchiveProduct = async (req, res) => {
    const token = req.headers.authorization;
    const userData = auth.decodeToken(token);
    const productId = req.params.productId;
    // const productType = req.params.productType;

    if (!userData.isAdmin)
        return res.json({
            message: "Only Admins can do archiving/unarchiving",
        });

    try {
        let collectionToBrowse = Product;
        // if (productType == "singleProduct") collectionToBrowse = Product;
        // if (productType == "bundleProduct") collectionToBrowse = BundleProduct;

        const productToBeUpdated = await collectionToBrowse.findById(productId);
        if (!productToBeUpdated)
            return res.json({
                message: `No Product with ID ${productId} Found`,
            });

        const changeArchiveStatus = {
            isAvailable: !productToBeUpdated.isAvailable,
        };

        await collectionToBrowse.findByIdAndUpdate(productId, changeArchiveStatus, {
            new: true,
        });

        res.json({ message: "Product Archived" });
    } catch (error) {
        res.json({ message: error.message });
    }
};
// End of ADMIN ONLY-------------------------------------------------------

// GET/View Available Products
module.exports.getAvailableProdcuts = async (req, res) => {
    const productType = req.params.productType;
    const skipNumOfProducts = req.query.skip ? Number(req.query.skip) : 0;
    const limitGetProducts = req.query.limit ? Number(req.query.limit) : 10;
    try {
        let collectionToBrowse = Product;
        // if (productType == "singleProduct") collectionToBrowse = Product;
        // if (productType == "bundleProduct") collectionToBrowse = BundleProduct;

        const availableProducts = await collectionToBrowse
            .find({
                isAvailable: true,
            })
            .skip(skipNumOfProducts)
            .limit(limitGetProducts);

        if (!availableProducts.length) return res.json({ message: "There are no products" });
        res.json(availableProducts);
    } catch (error) {
        res.json({ message: error.message });
    }
};

// ADD or REMOVE to/from CART
module.exports.addOrRemoveItems = async (req, res) => {
    const token = req.headers.authorization;
    const userData = auth.decodeToken(token);
    // if (userData.isAdmin)
    //     return res.json({ message: "Admins can't buy products." });

    const actionType = req.params.actionType;
    const product = req.body.product;
    const quantity = req.body.quantity;

    try {
        let productToBeAddedOrRemoved = await Product.findById(product.productId);
        if (!productToBeAddedOrRemoved)
            productToBeAddedOrRemoved = await BundleProduct.findById(product.productId);

        if (!productToBeAddedOrRemoved) return res.json({ message: "Product not Found" });

        const userCart = await Cart.findById(userData.cart);
        // .populate({
        //     path: "items",
        //     model: "Product",
        // });

        let itemPrice = "totalPrice";
        if (productToBeAddedOrRemoved.totalPrice == 0) itemPrice = "originalPrice";

        if (actionType == "removeFromCart") {
            const existingItemIndex = userCart.items.findIndex(
                (item) => item.productId == product.productId
            );
            const itemExistsIsCart = existingItemIndex !== -1;

            let updatedCartItems;
            if (itemExistsIsCart) {
                const existingItem = userCart.items[existingItemIndex];
                if (existingItem.quantity == 1) {
                    updatedCartItems = userCart.items.filter(
                        (item) => item.productId != product.productId
                    );
                } else {
                    const updatedItem = {
                        ...existingItem,
                        quantity: existingItem.quantity - 1,
                    };
                    updatedCartItems = [...userCart.items];
                    updatedCartItems[existingItemIndex] = updatedItem;
                }

                const numberOfCartItems = updatedCartItems.reduce((current, item) => {
                    return current + item.quantity;
                }, 0);
                userCart.items = updatedCartItems;
                userCart.totalItems = numberOfCartItems;
            }
        }
        //
        else if (productToBeAddedOrRemoved.stock == 0 || !productToBeAddedOrRemoved.isAvailable) {
            return res.json({ message: "Product Out of Stock or Unavailable" });
        }
        //
        else if (actionType == "addToCart") {
            const existingItemIndex = userCart.items.findIndex(
                (item) => item.productId == product.productId
            );

            const itemExistsIsCart = existingItemIndex !== -1;

            let updatedCartItems;
            if (itemExistsIsCart) {
                const existingItem = userCart.items[existingItemIndex];

                const updatedItem = {
                    ...existingItem,
                    quantity: existingItem.quantity + quantity,
                };

                updatedCartItems = [...userCart.items];
                updatedCartItems[existingItemIndex] = updatedItem;
            } else {
                const newProductToAdd = {
                    productImages: product.productImages,
                    variations: product.variations,
                    productId: product.productId,
                    productName: product.productName,
                    quantity: quantity,
                    originalPrice: product.originalPrice,
                    discount: product.discount,
                    totalPrice: product.totalPrice,
                };
                updatedCartItems = userCart.items.concat(newProductToAdd);
            }

            const numberOfCartItems = updatedCartItems.reduce((current, item) => {
                return current + item.quantity;
            }, 0);

            userCart.items = updatedCartItems;
            userCart.totalItems = numberOfCartItems;
        }
        const updatedCart = await userCart.save();
        res.json(updatedCart);
    } catch (error) {
        res.json({ message: error.message });
    }
};

module.exports.findProduct = async (req, res) => {
    const token = req.headers.authorization;
    const userData = auth.decodeToken(token);
    if (!userData) {
        ("Please Login First");
    }
    const productId = req.params.productId;

    try {
        const product = await Product.findById(productId);
        res.json(product);
    } catch (error) {
        res.json(error);
    }
};

module.exports.getFilteredProducts = async (req, res) => {
    const token = req.headers.authorization;
    const userData = auth.decodeToken(token);

    const skipNumOfProducts = req.query.skip ? Number(req.query.skip) : 0;
    const limitGetProducts = req.query.limit ? Number(req.query.limit) : 20;
    const filterBy = req.params.filter;

    let filter;
    let filterValue;
    if (filterBy === "discounted") {
        filter = "hasDiscount";
        filterValue = true;
    }
    if (filterBy === "popular") {
        filter = "totalBuyCount";
        filterValue = { $gte: 50 };
    }
    try {
        let collectionToBrowse = Product;
        // if (productType == "singleProduct") collectionToBrowse = Product;
        // if (productType == "bundleProduct") collectionToBrowse = BundleProduct;

        const availableProducts = await collectionToBrowse
            .find({
                isAvailable: true,
                [filter]: filterValue,
            })
            .skip(skipNumOfProducts)
            .limit(limitGetProducts);

        if (!availableProducts.length) return res.json({ message: "There are no products" });
        res.json(availableProducts);
    } catch (error) {
        res.json({ message: error.message });
    }
};
