const User = require("../models/userModel");
const Product = require("../models/productModel");
const BundleProduct = require("../models/bundleProductModel");
const Cart = require("../models/cartModel");
const Order = require("../models/orderModel");
const auth = require("../authorization");

// Checkout Cart
module.exports.checkOutSelectedItems = async (req, res) => {
    const token = req.headers.authorization;
    const userData = auth.decodeToken(token);
    // if (userData.isAdmin) return res.json({ message: "Admins can't do this." });

    const detailsOfCheckOut = req.body;
    try {
        const user = await User.findById(userData._id);
        const userCart = await Cart.findById(userData.cart);
        const itemsToCheckOut = detailsOfCheckOut.items;

        if (userCart.cartOwner._id != user.id)
            return res.json({ message: "This is not your Cart" });

        const itemsCheckedOut = [];
        for (let i = 0; i < itemsToCheckOut.length; i++) {
            itemsCheckedOut.push(itemsToCheckOut[i]);
        }

        for (const item in itemsCheckedOut) {
            let nameOfProduct = "productName";
            let product = await Product.findById(itemsCheckedOut[item].productId);
            if (!product) {
                product = await BundleProduct.findById(item.productId);
                nameOfProduct = "bundleName";
            }

            if (product.stock == 0 || !product.isAvailable)
                return res.json({
                    message: `${product[nameOfProduct]} is Out of Stock or Unavailable.`,
                });

            if (product.stock < item.quantity)
                return res.json({
                    message: `Not Enough stock for ${product[nameOfProduct]}. Your order is ${item.quantity}, and Available stock is only ${product.stock}`,
                });

            product.stock -= itemsCheckedOut[item].quantity;
            product.totalBuyCount += itemsCheckedOut[item].quantity;
            await product.save();

            let paymentStatus;
            if (detailsOfCheckOut.paymentMethod !== "Cash On Delivery") {
                paymentStatus = true;
            } else {
                paymentStatus = false;
            }

            const newOrder = new Order({
                orderedBy: user.id,
                productOrdered: itemsCheckedOut[item].productName,
                order: itemsCheckedOut[item],
                totalAmoutToBePaid: itemsCheckedOut[item].totalPrice,
                paymentMethod: detailsOfCheckOut.paymentMethod,
                isPaid: paymentStatus,
            });

            const indexOfOrder = userCart.items.findIndex(
                (itemInCart) => itemInCart._id === itemsCheckedOut[item]._id
            );
            userCart.items.splice(indexOfOrder, 1);
            userCart.totalItems -= itemsCheckedOut[item].quantity;
            const placedOrder = await newOrder.save();
            await userCart.save();
            user.orders.push(placedOrder);
            await user.save();
        }
        res.json("Transaction Successful!");
    } catch (error) {
        res.json({ message: error.message });
    }
};

// Checkout specific items
// i'm gonna need a frontend for this crap to be more precise
module.exports.checkSpecificItems = async (req, res) => {
    const token = req.headers.authorization;
    const userData = auth.decodeToken(token);
    if (userData.isAdmin) return res.json({ message: "Admins can't do this." });

    const productId = req.params.productId;

    try {
        const user = await User.findById(userData._id);
        let product = await Product.findById(productId);

        if (!product) product = await BundleProduct.findById(productId);
        if (!product) return res.json({ message: "Product Not Found" });
        if (product.stock == 0 || !product.isAvailable)
            return res.json({ message: "Prodcut Out of Stock or Unavailable" });

        const newOrder = new Order({
            orderedBy: user.id,
            orders: { product },
        });
        await newOrder.save(); //  did the save here to take a snapshot of product's stock to indicate priority
        user.orders.push(newOrder);
        await user.save();

        product.stock -= 1;
        product.totalBuyCount += 1;
        if (product.stock == 0) product.isAvailable = false;
        await product.save();

        res.json({ message: "Order Placed" });
    } catch (error) {
        res.json({ message: error.message });
    }
};
