const User = require("../models/userModel");
const Cart = require("../models/cartModel");
const Address = require("../models/addressModel");
const bcrypt = require("bcrypt");
const auth = require("../authorization");

module.exports.checkEmail = async (req, res, next) => {
    try {
        const users = await User.find({ email: req.body.email }); // Will handle regex on the fontend side.
        if (users.length > 0) return res.json({ message: "Email Is Taken" });
        next();
    } catch (error) {
        res.json({ message: error.message });
    }
};

module.exports.createUser = async (req, res) => {
    const input = req.body;

    try {
        const newCart = new Cart({
            items: [],
        });
        const userCart = await newCart.save();

        const hashedPassword = await bcrypt.hash(input.password, 10);
        const newUser = new User({
            email: input.email,
            password: hashedPassword,
            firstName: input.firstName,
            lastName: input.lastName,
            mobileNum: input.mobileNum,
            address: {
                streetNumberAndName: input.streetNumberAndName,
                brgyDistrict: input.brgyDistrict,
                cityMunicipality: input.cityMunicipality,
                postalCode: input.postalCode,
                country: input.country,
            },
            cart: userCart,
        });

        await newUser.save();
        userCart.cartOwner = newUser;
        await userCart.save();
        res.json({ message: "User Created" });
    } catch (error) {
        res.json({ message: error.message });
    }
};

module.exports.userLogin = async (req, res) => {
    const user = await User.findOne({ email: req.body.email });
    if (!user) return res.json({ message: "Email Not Registered" });

    try {
        const isMatch = await bcrypt.compare(req.body.password, user.password);
        if (!isMatch) return res.json({ message: "Incorrect Password" });
        const authToken = auth.authToken(user);

        res.json({ token: authToken, isAdmin: user.isAdmin });
    } catch (error) {
        res.json({ message: error.message });
    }
};

// ADD Address
module.exports.addAddress = async (req, res) => {
    const token = req.headers.authorization;
    const userData = auth.decodeToken(token);
    const input = req.body;

    try {
        const user = await User.findById(userData._id);
        if (!user) return res.json({ message: "User Not Found" });
        if (user.address.length > 1) return res.json({ message: "Can only have two addresses" });

        const newAddress = new Address({
            streetNumberAndName: input.streetNumberAndName,
            brgyDistrict: input.brgyDistrict,
            cityMunicipality: input.cityMunicipality,
            postalCode: input.postalCode,
            country: input.country,
        });

        user.address.push(newAddress);
        await user.save();
        res.json({ message: "Address Added" });
    } catch (error) {
        res.json({ message: error.message });
    }
};

// UPDATE Address
module.exports.updateAddress = async (req, res) => {
    const token = req.headers.authorization;
    const userData = auth.decodeToken(token);
    const addressSlot = req.params.addressSlot;
    const propertiesToBeUpdated = req.body;

    try {
        const user = await User.findById(userData._id);
        if (!user) return res.json({ message: "User Not Found" });
        const addressToUpdate = user.address[addressSlot];

        for (const reqProperty in propertiesToBeUpdated) {
            if (propertiesToBeUpdated[reqProperty]) {
                addressToUpdate[reqProperty] = propertiesToBeUpdated[reqProperty];
            }
        }
        await user.save();
        res.json({ message: "Address Update" });
    } catch (error) {
        res.json({ message: error.message });
    }
};

// DELETE Address
module.exports.deleteAddress = async (req, res) => {
    const token = req.headers.authorization;
    const userData = auth.decodeToken(token);
    const addressSlot = req.params.addressSlot;

    try {
        const user = await User.findById(userData._id);
        if (!user) return res.json({ message: "User Not Found" });

        if (user.address.length == 1)
            return res.json({
                message: "You need to have at least one address",
            });

        user.address.splice(addressSlot, 1);
        await user.save();
        res.json({ message: "Address Deleted" });
    } catch (error) {
        res.json({ message: error.message });
    }
};

// ADMIN ONLY ACCESSIBLE----------------------------------------------------------------------------
module.exports.getAllUsers = async (req, res) => {
    const token = req.headers.authorization;
    const userData = auth.decodeToken(token);
    if (!userData.isAdmin) return res.json({ message: "You are not an Admin" });

    try {
        const allUsers = await User.find({}, "-password").populate({
            path: "orders",
            model: "Order",
        });
        // .populate({
        //     path: "items",
        //     model: "Product",
        // });
        // .populate("cart")
        // .populate("postedProducts");
        // .populate("orders");
        if (!allUsers)
            return res.json({
                message: "There Are No Users of Your App/Website. Sad.",
            });
        allUsers;
        res.json(allUsers);
    } catch (error) {
        res.json({ message: error.message });
    }
};

module.exports.getOneUser = async (req, res) => {
    const token = req.headers.authorization;
    const userData = auth.decodeToken(token);

    try {
        if (userData) {
            const userDetails = { ...userData };
            userDetails.password = "********";
            res.json(userDetails);
        } else {
            res.json({ message: "Something went wrong!" });
        }
    } catch (error) {
        res.json({ message: error.message });
    }
};

// REFACTOR FOR ADMIN @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// module.exports.getOneUser = async (req, res) => {
//     const token = req.headers.authorization;
//     const userData = auth.decodeToken(token);
//     if (!userData.isAdmin) return res.json({ message: "You are not an Admin" });
//     const userId = req.params.userId;

//     try {
//         const user = await User.findById(userId);
//         // .populate("cart");
//         if (!user) return res.json({ message: "User Not Found" });
//         res.json(user);
//     } catch (error) {
//         res.json({ message: error.message });
//     }
// };

module.exports.changeRoleOfUser = async (req, res) => {
    const token = req.headers.authorization;
    const userData = auth.decodeToken(token);
    if (!userData.isAdmin) return res.json({ message: "You are not an Admin" });

    try {
        const userToBeUpdated = await User.findById(req.params.userId);
        if (!userToBeUpdated) return res.json({ message: "User Not Found" });

        const changeRole = {
            isAdmin: !userToBeUpdated.isAdmin,
        };

        const updatedUser = await User.findByIdAndUpdate(userToBeUpdated, changeRole, {
            new: true,
        });
        res.json({ message: updatedUser });
    } catch (error) {
        res.json({ message: error.message });
    }
};
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ NEWLY ADDED @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

module.exports.updateUser = async (req, res) => {
    const token = req.headers.authorization;
    const userData = auth.decodeToken(token);

    if (!userData.isAdmin)
        // || userData._id != req.params.userId
        return res.json({ message: "You are not Authorized" });

    try {
        const fieldsToBeUpdated = req.body;

        const userToBeUpdated = await User.findById(req.params.userId);

        for (const reqProperty in fieldsToBeUpdated) {
            if (fieldsToBeUpdated[reqProperty]) {
                if (reqProperty === fieldsToBeUpdated.userId) {
                    continue;
                }
                userToBeUpdated[reqProperty] = fieldsToBeUpdated[reqProperty];
            }
        }
        await userToBeUpdated.save();
        res.json({ message: "Update Successful!" });
    } catch (error) {
        res.json({ message: error.message });
    }
};

module.exports.appraiseAccount = async (req, res) => {
    const token = req.headers.authorization;
    const userData = auth.decodeToken(token);
    try {
        if (!userData.isAdmin) return res.json({ message: "User Not Found" });
        res.json({ isAdmin: userData.isAdmin });
    } catch (error) {
        res.json({ message: error.message });
    }
};

module.exports.getUserCart = async (req, res) => {
    const token = req.headers.authorization;
    const userData = auth.decodeToken(token);

    try {
        const userCart = await Cart.findById(userData.cart).populate({
            path: "items",
            model: "Product",
        });
        res.json(userCart);
    } catch (error) {
        res.json({ message: error.message });
    }
};
